package com.example.digitalbooking;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    private TextView tv_information;
    private EditText ed_name, ed_pass;
    private Button bn_login;
    ArrayList<UserModel> userModelArrayList;

    public static String keyIsLogged = "is_logged";
    public static String keyUsername = "username";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tv_information = findViewById(R.id.tv_enter_your_information);
        ed_name = findViewById(R.id.ed_enter_user_name);
        ed_pass = findViewById(R.id.ed_enter_user_pass);
        bn_login = findViewById(R.id.bn_login);

        userModelArrayList = new ArrayList<>();
        userModelArrayList.add(new UserModel("razan", "123"));
        userModelArrayList.add(new UserModel("rawan", "123"));
        userModelArrayList.add(new UserModel("ayat", "123"));
        userModelArrayList.add(new UserModel("ahmad", "123"));

    }

    protected void login(View v) {
        if (validate()) {
            if (isUser()) {
                sheredpreferences();
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);

            } else {
                Toast.makeText(this, "Yor name and pasword not correct", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private boolean validate() {

        boolean isValid = true;

        if (TextUtils.isEmpty(ed_name.getText().toString())) {
            ed_name.setError("Enter the Name");
            isValid = false;
        }
        if (TextUtils.isEmpty(ed_pass.getText().toString())) {
            ed_pass.setError("Enter the password");
            isValid = false;
        }
        return isValid;
    }

    private boolean isUser() {


        for (UserModel userModel : userModelArrayList) {
            if (ed_name.getText().toString().trim().equals(userModel.getNmae()) && ed_pass.getText().toString().trim().equals(userModel.getPassword())) {
                return true;
            }

        }
        return false;
    }

    private void sheredpreferences() {
        SharedPreferences shar = getSharedPreferences(
                getString(R.string.preference_file_key),MODE_PRIVATE);
        SharedPreferences.Editor pen = shar.edit();
        pen.putString(keyUsername, ed_name.getText().toString());
        pen.putBoolean(keyIsLogged, true);
        pen.commit();
    }

}

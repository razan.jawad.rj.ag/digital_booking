package com.example.digitalbooking;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import static com.example.digitalbooking.LoginActivity.keyIsLogged;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SharedPreferences shar = getSharedPreferences(
                getString(R.string.preference_file_key),MODE_PRIVATE);
        final boolean islogedTrue = shar.getBoolean(keyIsLogged, false);

        {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (islogedTrue) {
                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();

                    }
                }
            }, 3000);
        }


    }
}

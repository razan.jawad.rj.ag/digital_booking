package com.example.digitalbooking;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import static com.example.digitalbooking.LoginActivity.keyUsername;

public class ResultActivity extends AppCompatActivity {
private TextView tv_number,tv_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        tv_number=findViewById(R.id.tv_number);
        tv_text=findViewById(R.id.tv_When_is_your_turn);
        //get vaule from extra
      int number=getIntent().getExtras().getInt("theNumber");
        String text=getIntent().getExtras().getString("theText");

        //put the values in textView
        tv_number.setText(String.valueOf(number));
        SharedPreferences shar=getSharedPreferences(getString(R.string.preference_file_key),MODE_PRIVATE);

       String textShar= shar.getString(keyUsername,"user");
        tv_text.setText("Welcome" +" "+textShar);
    }
}

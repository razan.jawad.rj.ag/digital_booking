package com.example.digitalbooking;

public class TicketModel {
    private  int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public TicketModel(int number) {
        this.number = number;
    }
}

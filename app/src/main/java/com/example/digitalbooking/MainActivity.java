package com.example.digitalbooking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Button bn1, bn2, bn3, bn4, bn5, bn6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bn1 = findViewById(R.id.bn_accounts_management);
        bn2 = findViewById(R.id.bn_customer_service_officer);
        bn3 = findViewById(R.id.bn_cheques);
        bn4 = findViewById(R.id.bn_loans);
        bn5 = findViewById(R.id.bn_transfers);
        bn6 = findViewById(R.id.bn_vip);

    }

    public void click_any_button_in_main(View v) {
        Button  button = (Button) v;
        Intent i = new Intent(MainActivity.this, ResultActivity.class);

        if (v.getId() == R.id.bn_accounts_management) {
            i.putExtra("theNumber", 1);
            i.putExtra("theText", button.getText().toString());

        }
        else if(v.getId()==R.id.bn_customer_service_officer)
    {
        i.putExtra("theNumber", 2);
        i.putExtra("theText", button.getText().toString());
    }
         else if(v.getId()==R.id.bn_cheques)
        {
            i.putExtra("theNumber", 3);
            i.putExtra("theText", button.getText().toString());
        }
        else if(v.getId()==R.id.bn_loans)
        {
            i.putExtra("theNumber", 4);
            i.putExtra("theText", button.getText().toString());
        }
        else if(v.getId()==R.id.bn_transfers)
        {
            i.putExtra("theNumber", 5);
            i.putExtra("theText", button.getText().toString());
        }
        else if(v.getId()==R.id.bn_vip)
        {
            i.putExtra("theNumber", 6);
            i.putExtra("theText", button.getText().toString());
        }

        startActivity(i);
    }

}
